<?hh

abstract class User {
  abstract const type T;
  abstract const type T as int;
  abstract const type T super string;
}
