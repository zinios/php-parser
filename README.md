# HackLang Parser

This project is a parser for [HackLang](http://www.hacklang.org) written in PHP. It builds an Abstract Syntax Tree (AST) from the given HackLang source code. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See maintenance & development for notes on how to maintain and add new features to this project.

### Prerequisites

- PHP Version 7+ 

[Install PHP for Linux](http://php.net/manual/en/install.unix.debian.php)

[Install PHP for Windows](http://php.net/manual/en/install.windows.php)

- ext-dom extension

```sudo apt-get instal php-xml```

- KMYACC

[Install KMYACC](https://github.com/moriyoshi/kmyacc-forked)

- Composer  

[Install Composer for Linux](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) 

[Install Composer for Windows](https://getcomposer.org/doc/00-intro.md#installation-windows) 

### Installing

1) Clone the project 

```
git clone https://adelmaratova@bitbucket.org/zinios/php-parser.git
cd ./php-parser

```
2) Download the dependencies
```
php composer.phar install
```
3) Build new parser 
```
./grammar/rebuildParser.sh 
```
4) Run parser on your code, e.g.
```
./phack example/example_alias.php 
```

#### Example 

Input (example/example_alias.php):
```
<?hh type myType = int;
```
Output:
```
Array
(
    [0] => PhpLang\Phack\PhpParser\Node\Stmt\Alias Object
        (
            [name] => myType
            [type] => int
            [attributes:protected] => Array
                (
                            [startLine] => 1
                            [endLine] => 1
                )

        )

)
```
## Running the tests

###PHPUnit

The Hack Parser contains the unit tests for each hack specific feature. To run the tests:

1) [Install PHPUnit](https://github.com/sebastianbergmann/phpunit#installation)

2) Run:
```
phpunit ./path-to-this-project/tests/some-specific-test
```
###Coding style test

There are some specific coding standard, that is being used in Zinios. To check your code against it:

1) Install [PHP-codesniffer](https://github.com/squizlabs/PHP_CodeSniffer)

2) Run

```
phpcs standard=ZiniosHack /path/to/your/file
```

### Adding new tests

The Hack Parser contains the tests for each hack specific feature. In order to facilitate future maintenance and development of the project it is recommended to add the corresponding tests every time the new feature being added.

Here is the guide on how to [write useful tests for PHPUnit.](https://jtreminio.com/2013/03/unit-testing-tutorial-part-2-assertions-writing-a-useful-test-and-dataprovider/)

## Maintenance and development 

[Hacklang.org](http://hacklang.org/) adds a new feature to Hack approximately every 8 weeks. Therefore in order for Hack parser to be able to parse the latest pieces of code, the new features should be added to the project.

###How to add a feature to Hack-parser?

Depending on the feature the procedure may vary, but generally this is the pattern. 

Note: If token for a feature is already existing, start from point 2.

1. Create a token T_SOMEOPERATOR at Grammar/rebuildParser.sh
2. Add it to Lexer/HackLang.php.
3. Describe reduction rule at Grammar/Hack-Extra.y.
4. Create a class, that is extending phack/Node for this operator.
5. Create an example input in Example folder, which includes your feature and try to run it. **(In case it doesn't work check points 1-4 again and make sure the reduction rule describes all the possible use cases).** 
6. Important: write a test for it. When implementing new feature the old one may break.