<?php

namespace PhpLang\Phack\PhpParser\Node;

class ConstTypeConstraint extends \PhpParser\NodeAbstract {
	use GetType;
	const AS = 1;
	const SUPER = 2;
	
	/** @var int Constraint: self::AS or self::SUPER */
	public $rel;
	
	/** @var string | pNode\Name Type As/Super (co/contra variance) */
	public $constraint;
	
	/**
	 * Constructs a name node.
	 *
	 * @param int $rel
	 *        	self::AS or self::SUPER
	 * @param
	 *        	string | pNode\Name $constraint Type As/Super (co/contra variance)
	 * @param array $attributes
	 *        	Additional attributes
	 */
	public function __construct($rel, $constraint, array $attributes = array()) {
		parent::__construct ( $attributes );
		$this->rel = $rel;
		$this->constraint = $constraint;
	}
	public function getSubNodeNames() {
		return array (
				'rel',
				'constraint' 
		);
	}
}
