<?php

namespace PhpLang\Phack\PhpParser\Node;

use \PhpParser\Node as pNode;

class ConstType extends \PhpParser\NodeAbstract
{
	    use GetType;
	
	    const ABSTRACT = 'abstract';
	    const CONCRETE = 'concrete';
	
	    /** @var string|Name name of the type constant */
	    public $name;
	
	    /** @var int this::ABSTRACT or CONCRETE Kind of type constant */
	    public $kind;
	
	    /** @var string[] Constraint */
	    public $constraint;
	
	    /** @var string[] Value*/
	    public $value;
	
	    /**
	     * Constructs a name node.
	     *
	     * @param string|Name  $basename                           Base name of Constant Type
	     * @param int this::ABSTRACT or CONCRETE   Kind of type constant
	     * @param string[] Constraint                                      Constraint of the type constant
	     * @param string Value                                             Value of the type constant
	     */
	    public function __construct($name, $kind, array $subNodes = array(), array $attributes = array()) {
		        parent::__construct($attributes);
		        $this->name = $name;
		        $this->kind = $kind;
		        $this->value = isset($subNodes['value']) ? $subNodes['value'] : array();
		        $this->constraint = isset($subNodes['constraint']) ? $subNodes['constraint'] : array();
		    }
		
		    public function getSubNodeNames() {
			        return array('name','kind', 'constraint','value');
			    }
			}
